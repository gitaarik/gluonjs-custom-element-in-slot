import { GluonElement, html } from '@gluon/gluon/gluon.js';

class MyElement extends GluonElement {

  get template() {
    return html`
      <div>
        <strong>Wow!</strong>
        <slot></slot>
      </div>
    `;
  }

}

customElements.define(MyElement.is, MyElement);

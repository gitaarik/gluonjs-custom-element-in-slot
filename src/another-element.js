import { GluonElement, html } from '@gluon/gluon/gluon.js';

class AnotherElement extends GluonElement {

  get template() {
    return html`<i>Hello?</i>`;
  }

}

customElements.define(AnotherElement.is, AnotherElement);

import { GluonElement, html } from '@gluon/gluon/gluon.js';
import './my-element.js';
import './another-element.js';

class GluonjsTemplate extends GluonElement {

  get template() {
    return html`
      <my-element>
        Some content here
        <another-element></another-element>
        Some more content
      </my-element>
    `;
  }

}

customElements.define(GluonjsTemplate.is, GluonjsTemplate);
